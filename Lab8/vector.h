
#include <vector>
#include <random>
#include <chrono>
using namespace std;
#define num_thrd1 2

void* MatrixVectorMultiplication(void* slice);
/******A n element Vector*******/
class N_Dimensional_Vector{
    public:
    const int size;
    vector<int> *vec = new vector<int>();
    friend void* MatrixVectorMultiplication(void* slice);
    N_Dimensional_Vector(int value) : size(value) {
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937 generator(seed);
        for (int i = 0; i < size; i++){
            vec->push_back(generator() % 6);
        }
    }
};

