#include <iostream>
#include <vector>
#include <chrono>
#include <random>
#include <pthread.h>
#include <sys/time.h>
#include "vector.h"
#include "matrix.h"
using namespace std;


#define num_thrd 2


int main()
{
	pthread_t* thread;
	struct timeval start, end;
	long mtime, seconds, useconds;    

	cout<<"With "<<num_thrd<<" threads:-"<<endl;
	cout<<"Matrix Multiplication with Vector"<<endl;
	thread = (pthread_t*) malloc(num_thrd*sizeof(pthread_t));
	gettimeofday(&start, NULL);
	for (int i = 0; i < num_thrd; i++)
	{
	  // creates each thread working on its own slice of i
	  if (pthread_create (&thread[i], NULL, MatrixVectorMultiplication, (void*)i) != 0 )
	  {
	    perror("Can't create thread");
	    free(thread);
	    exit(-1);
	  }
	}

	// main thead waiting for other thread to complete
	for (int i = 0; i < num_thrd; i++)
	  pthread_join (thread[i], NULL);
	gettimeofday(&end, NULL);
	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
	cout<<"Elapsed time: "<<mtime<<endl;

	cout<<endl<<"Matrix Multiplication with Matrix"<<endl;
	thread = (pthread_t*) malloc(num_thrd*sizeof(pthread_t));
	gettimeofday(&start, NULL);
	for (int i = 0; i < num_thrd; i++)
	{
	  // creates each thread working on its own slice of i
	  if (pthread_create (&thread[i], NULL, MatrixMatrixMultiplication, (void*)i) != 0 )
	  {
	    perror("Can't create thread");
	    free(thread);
	    exit(-1);
	  }
	}

	// main thead waiting for other thread to complete
	for (int i = 0; i < num_thrd; i++)
	  pthread_join (thread[i], NULL);
	gettimeofday(&end, NULL);
	seconds  = end.tv_sec  - start.tv_sec;
	useconds = end.tv_usec - start.tv_usec;
	mtime = ((seconds) * 1000 + useconds/1000.0) + 0.5;
	cout<<"Elapsed time: "<<mtime<<endl;
	pthread_exit(NULL);
	getchar();
	return 0;
}

