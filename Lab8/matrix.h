
#include <vector>
#include <random>
#include <chrono>

#define num_thrd2 2
using namespace std;

void* MatrixMatrixMultiplication(void* slice);

/******A m x n element Vector*******/
class M_Cross_N_Matrix{
    public:
    const int size;
    const int row;
    const int col;
    vector<int> *vec = new vector<int>();
    friend void* MatrixMatrixMultiplication(void* slice);
    M_Cross_N_Matrix(int x, int y) : row(x), col(y), size(x*y){
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::mt19937 generator(seed);
        for (int i = 0; i < size; i++){
            vec->push_back(generator() % 6);
        }
    }
};
